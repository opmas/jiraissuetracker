import os
from web_app import create_app
from flask_script import Manager, Server
from flask_migrate import Migrate, MigrateCommand
from utils import log

"""
This module is the app's entry point.
Using Flask-Script, three command-line commands are available:
    runserver:      launches the container.
    db:             provides access to Alembic's DB migration services.
    redresh_schema: rebuilds the DB schema from scratch.
"""

# Initialize logging services
log.init_logging(os.path.abspath(os.path.dirname(__file__)))

# Initialize web-app container and all required app dependencies
app = create_app(os.getenv("FLASK_CONFIG") or "default")

# IMPORTANT: the following import has to come AFTER the app had been initialized. Otherwise, the db and db_operations
# global variables would not be initialized (i.e., equal to None)
from web_app import db, apscheduler

# Add support for Flask-Script wrapper (commandline and admin facilities)
manager = Manager(app)
server = Server(host="0.0.0.0", port=5101, use_reloader=False)
manager.add_command("runserver", server)

# Command: database migration management (Alembic wrapper)
# One way to register a new script command...
migrate = Migrate(app, db)
manager.add_command("db", MigrateCommand)

# Command: refresh database schema
# Another way to register a new script command (using decorators)
@manager.command
def refresh_schema():
    db.drop_all()
    db.create_all()
    print ">>> Done"


if __name__ == "__main__":
    app.debug = False
    apscheduler.start()
    manager.run()


