(function(W, D)
{
    $(D).ready(function()
    {
        var time = new Date().getTime();
        $(document.body).bind("mousemove keypress", function(e) {
            time = new Date().getTime();
        });

        function refresh() {
            if(new Date().getTime() - time >= 10000)
                window.location.reload(true);
             else
                setTimeout(refresh, 2000);
     }

     setTimeout(refresh, 2000);

    });

})(window, document);
