(function($, W, D, undefined)
{
    $(D).ready(function()
    {
         $("#add-issue-form").validate(
         {
             onkeyup: false,
             rules:
             {
                new_issue_id:
                {
                    required: true,
                    minlength: 6,
                    "remote":
                    {
                      url: '/issues/validate',
                      type: "post",
                      /*
                      data:
                      {
                          issue_id: $('#add-issue-form :input[name="new_issue_id"]').val()
                      }
                      */
                    }
                }
             },
             messages:
             {
                 new_issue_id:
                 {
                    required: "Please enter a valid Jira issue ID (e.g. MOOMOO-123)",
                    remote: jQuery.validator.format("{0} does not exists or already registered")
                 },
             } /*,
             submitHandler: function(form)
             {
                console.log("@@@ form submitted");
                form.submit();
             }
             */
         });
    });

})(jQuery, window, document);
