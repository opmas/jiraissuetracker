import logging
from flask import render_template, request, url_for, flash, redirect
from flask_login import login_user, logout_user, login_required
from . import auth_view
from .forms import LoginForm, SignUpForm
from services.auth import create_user, fetch_user_by_email, verify_password


logger = logging.getLogger(__name__)


@auth_view.route("/login", methods=["GET"])
def get_login():

    login_form = LoginForm()
    return render_template("auth/login.html", form=login_form)


@auth_view.route("/login", methods=["POST"])
def post_login():

    login_form = LoginForm()

    # Login form fields have NOT passed validation
    if not login_form.validate_on_submit():
        return render_template("auth/login.html", form=login_form)

    # Attempt to fetch the user from the DB by the specified email
    user = fetch_user_by_email(login_form.email.data)

    # If user exists in the database and has specified a valid password, log him / her in
    if user and verify_password(user, login_form.password.data):
        login_user(user, login_form.remember_me.data)
        return redirect(request.args.get("next") or url_for("stats_view.get_issues_being_tracked"))
    # Either user's email or password are invalid
    else:
        flash("Invalid email or password. Please try again.")
        return redirect(url_for(".get_login"))


@auth_view.route("/logout", methods=["GET", "POST"])
@login_required
def get_logout():

    logout_user()
    flash("You have been logged-out")
    return redirect(url_for(".get_login"))


@auth_view.route("/signup", methods=["GET"])
def get_signup():

    signup_form = SignUpForm()
    return render_template("auth/signup.html", signup_form=signup_form)


@auth_view.route("/signup", methods=["POST"])
def post_signup():

    signup_form = SignUpForm()

    # Sign-up form fields have NOT passed validation
    if not signup_form.validate_on_submit():
        return render_template("auth/signup.html", signup_form=signup_form)

    # Email is not already taken (i.e., user exists in the database)
    email_in_use = fetch_user_by_email(signup_form.email.data)

    if email_in_use:
        flash("Email is already taken. Please try a different one.")
    # The two password (passowrd and repeat password) do not match
    elif signup_form.password.data != signup_form.re_password.data:
        flash("The passwords entered do not match. Please try again.")
    # All is good, create the new user
    else:
        create_user(name=signup_form.name.data,
            email=signup_form.email.data,
            password=signup_form.password.data,
            address=signup_form.address.data)
        flash("Thank you, {nick}! Please login.".format(nick=signup_form.name.data))
        logger.info("User {nick} has successfuly registered".format(nick=signup_form.name.data))
        return redirect(url_for(".get_login"))

    return render_template("auth/signup.html", signup_form=signup_form)
