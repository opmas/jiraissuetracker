from flask_wtf import Form
from wtforms import StringField, PasswordField, BooleanField, SubmitField
from wtforms.validators import InputRequired, Length, Email, Optional


class LoginForm(Form):
    """
    Simple Login form
    """

    email = StringField("Email", validators = [InputRequired(), Length(1, 64), Email()])
    password = PasswordField("Passowrd", validators = [InputRequired()])
    remember_me = BooleanField("Keep me logged in")
    submit = SubmitField("Log In")


class SignUpForm(Form):
    """
    Simple Sign-Up form
    """

    name = StringField("Nick Name", validators = [InputRequired(), Length(1, 32)])
    email = StringField("Email", validators = [InputRequired(), Length(1, 64), Email()])
    address = StringField("Address", validators = [Optional(), Length(1, 128)])
    password = PasswordField("Passowrd", validators = [InputRequired(), Length(6, 16)])
    re_password = PasswordField("Retype Password", validators = [InputRequired(), Length(6, 16)])
    submit = SubmitField("Sign Up!")