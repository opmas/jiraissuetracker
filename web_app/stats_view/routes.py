from flask import render_template, request, flash, Response
from flask_login import login_required
from services.issues import fetch_all_issues, fetch_all_users, retrieve_issue_from_jira, upsert_issue
from . import stats_view


@stats_view.route("/issues", methods=["GET"])
@stats_view.route("/", methods=["GET"])
@login_required
def get_issues_being_tracked():

    issues = fetch_all_issues()
    return render_template("stats/list_issues.html",issues=issues)


@stats_view.route("/users", methods=["GET"])
@login_required
def get_worklog_reporting_users():

    users = fetch_all_users()
    return render_template("stats/list_users.html",users=users)


@stats_view.route("/issues/add", methods=["POST"])
@login_required
def post_add_issue_for_tracking():

    new_issue = None
    new_issue_id = request.form.get("new_issue_id")
    if new_issue_id:
        jira_issue = retrieve_issue_from_jira(new_issue_id)
        if jira_issue:
            new_issue = upsert_issue(issue_id=new_issue_id, updated=jira_issue.fields.updated, summary=jira_issue.fields.summary,
                                     status=jira_issue.fields.status.name, overall_worklog=jira_issue.fields.aggregatetimespent)

    if new_issue:
        flash("Issue {id} is now being tracked".format(id=new_issue.issue_id))
    else:
        flash("Issue could not be added. Please try again in a little while")

    issues = fetch_all_issues()
    return render_template("stats/list_issues.html",issues=issues)


@stats_view.route("/issues/validate", methods=["POST"])
@login_required
def get_validate_issue_before_adding():

    """
    This function is AJAX invoked by the JQuery Validate plugin used to provide the user with feedback on the
    validity of the issue id being added for tracking.
    :return: plain text "true" or "false", as expected by JQuery's Validate plugin
    """
    new_issue_id = request.form.get("new_issue_id")
    if new_issue_id:
        jira_issue = retrieve_issue_from_jira(new_issue_id)
        if jira_issue:
                return Response("true", mimetype="text/plain")

    return Response("false", mimetype="text/plain")
