import logging
from flask import Flask
from flask_apscheduler import APScheduler
from flask_bootstrap import Bootstrap
from flask_login import LoginManager
from flask_moment import Moment
from config import config


VERSION = "0.0.1a"

# Web app's exposed services (package globals)
db = None
apscheduler = None
bootstrap = None
login_manager = None
moment = None
global_config = None

logger = logging.getLogger(__name__)


def create_app(config_name):

    global db, apscheduler, bootstrap, login_manager, moment, global_config

    # Init vanilla Flask app (no dependencies)
    app = Flask(__name__)
    app.config.from_object(config[config_name])

    # Externalize app configuration so that other services not necessarily tied to the app can share a single configuration object
    global_config = app.config

    # Add SQL DB dependency
    if app.config.get("SQLALCHEMY_DATABASE_URI") is not None:
        from flask_sqlalchemy import SQLAlchemy
        db = SQLAlchemy(app)
        db.init_app(app)
    else:
        logger.error("SQLALCHEMY_DATABASE_URI is not defined in configuration file. Aborting.")
        raise EnvironmentError(">>> SQL database not configured")

    # Add Bootstrap dependency
    bootstrap = Bootstrap()
    bootstrap.init_app(app)

    # Add APScheduler dependency
    try:
        apscheduler = APScheduler()
        apscheduler.init_app(app)
    except Exception as e:
        logger.warn("APScheduler is (likely) already running <{0}>".format(e))

    # Add Login Manager dependency
    login_manager = LoginManager()
    # This is where the app will route if the user attempts to access an "authenticated only" page
    login_manager.login_view = "auth_view.get_login"
    login_manager.init_app(app)

    # Add Moment library (time formatting) dependency
    moment = Moment()
    moment.init_app(app)

    # Register template engine helper functions
    from utils.time import convert_jira_datetime_to_datetime, format_seconds_to_days
    app.add_template_global(len, name="len")
    app.add_template_global(convert_jira_datetime_to_datetime, name="convert_jira_datetime_to_datetime")
    app.add_template_global(format_seconds_to_days, name="format_seconds_to_days")

    # Register Jira Stats view routes
    from .stats_view import stats_view as stats_blueprint
    app.register_blueprint(stats_blueprint)

    # Register authentication related view routes
    from .auth_view import auth_view as auth_blueprint
    app.register_blueprint(auth_blueprint, url_prefix = "/auth")

    return app
