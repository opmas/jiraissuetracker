import os
from collections import namedtuple

basedir = os.path.abspath(os.path.dirname(__file__))


class Config:
    SECRET_KEY = os.environ.get('SECRET_KEY')
    SCHEDULER_EXECUTORS = {
        "default": { "type": "threadpool", "max_workers": 1 }
    }
    JOBS = [
        {
            "id":       "job_refresh_worklogs_serially",
            "func":     "services.scheduler:serial_scheduler",
            "args":     ("Blah!"),
            "trigger":  "interval",
            "seconds":  60
        }
    ]


class DevelopmentConfig(Config):
    DEBUG = True
    SECRET_KEY = os.environ.get("SECRET_KEY") or 't0p s3cr3t'
    SQLALCHEMY_DATABASE_URI = "sqlite:///" + os.path.join(basedir, "data/jira-stats.sqlite")
    ALEMBIC_SQLALCHEMY_DATABASE_URI = "sqlite:///" + os.path.join(basedir, "data/jira-stats.sqlite")
    JIRA_CONFIG = namedtuple("Jira", ["server", "username", "password"]) \
                            (server="http://jira.comm-it.com", username="username", password="password")
    SCHEDULER_VIEWS_ENABLED = True


class TestingConfig(Config):
    TESTING = True
    SECRET_KEY = os.environ.get("SECRET_KEY") or 'n0 s3cr3t'
    SQLALCHEMY_DATABASE_URI = os.environ.get("TEST_DATABASE_URL") or 'sqlite:///' + os.path.join(basedir, "data-test.sqlite")


class ProductionConfig(Config):
    SECRET_KEY = os.environ.get("SECRET_KEY")
    SQLALCHEMY_DATABASE_URI = os.environ.get("PROD_DATABASE_URL") or 'sqlite:///' + os.path.join(basedir, "data-prod.sqlite")

config = {
    "development":  DevelopmentConfig,
    "testing":      TestingConfig,
    "production":   ProductionConfig,
    "default":      DevelopmentConfig
}
