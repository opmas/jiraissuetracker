from jira import JIRA
from web_app import global_config

# "Eager" opening of a connection to the Jira Server. This pacakge level connection variable is later used by various
# services within this package.

jira_config = global_config["JIRA_CONFIG"]
jira_conn = JIRA(server=jira_config.server, basic_auth=(jira_config.username, jira_config.password))

# ToDo: introduce a Jira connection pool (concurrent jobs, refreshing stale connections)
