from collections import defaultdict
import logging
from .issues import fetch_all_issues, upsert_issue, retrieve_issue_from_jira


logger = logging.getLogger(__name__)


def serial_scheduler(*args):

    issues = fetch_all_issues()

    # Traverse all registered issues
    for issue in issues:

        jira_issue = retrieve_issue_from_jira(issue.issue_id)
        if not jira_issue:
            logger.warn("Issue {0} is no longer in Jira. Consider removing from watch list.".format(issue.issue_id))
            continue

        # Issue ID has been updated since the last refresh cycle. Need to reprocess.
        if jira_issue.fields.updated is not issue.updated:
            # Group (sum) worklogs based on reporter (user/author)
            grouped_worklog = defaultdict(int)
            for worklog in jira_issue.fields.worklog.worklogs:
                grouped_worklog[worklog.updateAuthor.name] += worklog.timeSpentSeconds
            # Update existing issue
            upsert_issue(use_issue=issue, updated=jira_issue.fields.updated, summary=jira_issue.fields.summary,
                         status=jira_issue.fields.status.name, overall_worklog=jira_issue.fields.aggregatetimespent,
                         grouped_worklogs=grouped_worklog)

    return