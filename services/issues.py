import logging
from jira import JIRAError
from web_app import db
from models.jira_issue import JiraIssue
from models.jira_work_log import JiraWorkLog
from . import jira_conn


logger = logging.getLogger(__name__)


def fetch_all_issues():

    try:
        issues = JiraIssue.query.order_by(JiraIssue.issue_id.asc()).all()
    except Exception as e:
        issues = None
        logger.error("Failed to fetch all issues <{0}>".format(e))

    return issues


def fetch_all_worklogs(issue_id):

    try:
        worklogs = JiraWorkLog.query.filter_by(issue_id=issue_id).all()
    except Exception as e:
        worklogs = None
        logger.error("Failed to fetch worklogs for issue-id {0} <{1}>".format(issue_id, e))

    return worklogs


def upsert_issue(issue_id=None, use_issue=None, **issue_properties):

    """
    Upserts (updates or inserts) an issue. The service is desgined as such that it does not need to change in any way
    if the model changes.

    :param issue_id: identifies the issue to be upsert-ed by its issue-id
    :param use_issue: provides the actual issue model object on which to operates. Overrides issue_id.
    :param issue_properties: any combination of issue model properties to be updated.
           NOTE: properties not defined by the model will be IGNORED.
    :return: Either the updated issue model, or the newly created issue model.
    """

    issue = use_issue if use_issue else fetch_single_issue(issue_id)

    # Update an existing issue
    if issue:
        for issue_property in issue_properties:
            if hasattr(issue, issue_property):
                setattr(issue, issue_property, issue_properties[issue_property])
    # Add an new issue
    else:
        issue = JiraIssue(issue_id=issue_id, **issue_properties)

    # Handle worklog updates
    if "grouped_worklogs" in issue_properties:
        issue = upsert_worklogs(use_issue=issue, grouped_worklogs_dict=issue_properties["grouped_worklogs"])

    # ToDo: PROPER error handling
    db.session.add(issue)
    db.session.commit()

    return issue


def upsert_worklogs(issue_id=None, use_issue=None, grouped_worklogs_dict=None):

    """
    Upserts (updates or inserts) a set of worklogs for a given issue.

    :param issue_id: identifies the issue to be upsert-ed by its issue-id
    :param use_issue: provides the actual issue model object on which to operates. Overrides issue_id.
    :param grouped_worklogs_dict: a dictionary of the updated worklog status for the given issue.
    :return: Updated issue model.
    """

    # Issue existence is guaranteed by the caller, as there is no point in invoking this service without having
    # a valid issue at hand.
    issue = use_issue if use_issue else fetch_single_issue(issue_id)

    # Check that there are updated worklogs to work with
    if (not grouped_worklogs_dict) or (len(grouped_worklogs_dict) == 0):
        return issue

    copy_of_grouped_worklogs_dict = dict(grouped_worklogs_dict)

    # Handle the update of *existing* log entities. Those need either be updated or deleted (in case the worklog in
    # Jira has been manually deleted).
    with db.session.no_autoflush:

        for existing_worklog in issue.worklogs:

            if existing_worklog.user in copy_of_grouped_worklogs_dict:
                existing_worklog.time_logged_seconds = copy_of_grouped_worklogs_dict[existing_worklog.user]
                del copy_of_grouped_worklogs_dict[existing_worklog.user]
            # Whenever a worklog exists in the DB but not in the updated worklog dictionary, assume that the worklog
            # entry had been deleted as of the last update.
            else:
                db.session.delete(existing_worklog)

        # Handle the creation of new worklog entities (those remaining for which an existing worklog entry could not be found)
        for new_user_worklog in copy_of_grouped_worklogs_dict:
            worklog = JiraWorkLog(issue_id=issue.issue_id, user=new_user_worklog, time_logged_seconds=copy_of_grouped_worklogs_dict[new_user_worklog])
            issue.worklogs.append(worklog)

    # Commit the changes if function was *not* called by some other function which is the originator and owner of "issue" object
    if not use_issue:
        db.session.add(issue)
        db.session.commit()

    return issue


def fetch_single_issue(issue_id):

    try:
        issue = JiraIssue.query.filter_by(issue_id=issue_id).one()
    except Exception as e:
        issue = None
        logger.error("Failed to fetch issue-id {0} <{1}>".format(issue_id, e))

    return issue


def fetch_all_users():
    try:
        users = db.session.query(JiraWorkLog.user, db.func.sum(JiraWorkLog.time_logged_seconds)) \
                          .group_by(JiraWorkLog.user).order_by(db.asc(JiraWorkLog.user)).all()
    except Exception as e:
        users = None
        logger.error("Failed to fetch all users {0}".format(e))

    return users


def retrieve_issue_from_jira(issue_id):
    try:
        jira_issue = jira_conn.issue(issue_id, fields="summary,assignee,worklog,status,updated,aggregatetimespent")
    except JIRAError as e:
        jira_issue = None
        logger.warn("Could not retrieve issue-id {0} from Jira <{1}>".format(issue_id, e))

    return jira_issue

def delete_issue(issue_id):
    JiraIssue.query.filter_by(issue_id=issue_id).delete()
    db.session.commit()
    return
