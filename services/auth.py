import logging
from werkzeug.security import check_password_hash
from web_app import db, login_manager
from models.user import User

logger = logging.getLogger(__name__)


def verify_password(user, password):
    is_valid = check_password_hash(user.password_hash, password)
    return is_valid


@login_manager.user_loader
def fetch_user_by_id(user_id):

    user_id = int(user_id)
    try:
        user = User.query.get(user_id)
    except:
        user = None

    return user


def fetch_user_by_email(email):

    try:
        user = User.query.filter_by(email=email).first()
    except:
        user = None

    return user


def create_user(name, address, email, password):

    new_user = User(name=name, address=address, email=email, password=password)
    db.session.add(new_user)
    db.session.commit()

    return new_user
