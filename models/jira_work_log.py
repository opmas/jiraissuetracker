from web_app import db


class JiraWorkLog(db.Model):
    __tablename__ = "worklogs"

    id = db.Column(db.Integer, primary_key=True) # Another way to declare a primary key
    issue_id = db.Column(db.String(256), db.ForeignKey("issues.issue_id"), index=True)
    user = db.Column(db.String(16), index=True, nullable=False)
    time_logged_seconds = db.Column(db.Integer, nullable=False)

    @property
    def serialize(self):
        dict = { "id":                  self.id,
                 "issue_id":            self.issue_id,
                 "user":                self.user,
                 "time_logged_secs":    self.time_logged_seconds }

        return dict