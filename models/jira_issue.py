from datetime import datetime
from web_app import db
from .jira_work_log import JiraWorkLog


class JiraIssue(db.Model):
    __tablename__ = "issues"

    id = db.Column(db.Integer)
    issue_id = db.Column(db.String(16), unique=True)
    summary = db.Column(db.String(256), nullable=False)
    status = db.Column(db.Enum("Open", "Ready", "Assigned", "Resolved", "Closed", name="ENUM_JIRA_STATUS"), nullable=True)
    overall_worklog = db.Column(db.Integer, default=0)
    updated = db.Column(db.String(32), nullable=False)
    worklogs = db.relationship(JiraWorkLog, backref="issue")

    __table_args__ = (
        db.PrimaryKeyConstraint("id"),
    )

    @property
    def serialize(self):
        dict = { "issue_id":        self.issue_id,
                 "summary":         self.summary,
                 "status":          self.status,
                 "overall_worklog": self.overall_worklog,
                 "updated":         self.updated }

        return dict

