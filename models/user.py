from datetime import datetime
from werkzeug.security import generate_password_hash
from flask_login import UserMixin
from web_app import db


class User(UserMixin, db.Model):
    __tablename__ = "users"

    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(48), nullable=False)
    address = db.Column(db.String(128), nullable=True)
    email = db.Column(db.String(128), nullable=False, unique=True)
    password_hash = db.Column(db.String(128), nullable=True)
    created = db.Column(db.DateTime, default=datetime.utcnow)

    @property
    def password(self):
        raise AttributeError(">>> for security reasons, 'password' is not a readable attribute")

    @password.setter
    def password(self, password):
        self.password_hash = generate_password_hash(password)

    @property
    def serialize(self):
        dict = { "id":              self.id,
                 "name":            self.name,
                 "address":         self.address,
                 "email":           self.email,
                 "created":         self.created }
        return dict