from datetime import datetime


def convert_jira_datetime_to_datetime(jira_datetime):
    return datetime.strptime(jira_datetime.split("+")[0], "%Y-%m-%dT%H:%M:%S.%f")


def format_seconds_to_days(seconds):
    seconds = seconds or 0
    minutes, seconds = divmod(int(seconds), 60)
    hours, minutes = divmod(minutes, 60)
    days, hours = divmod(hours, 8) # Workday in Jira is defined as 8 hours
    formatted_datetime = "%dd %dh %dm" % (days, hours, minutes)
    return formatted_datetime