import os
import json
import logging.config


def init_logging(base_dir):

    create_log_directory()
    with open(base_dir +'/logger.json', 'rt') as config_file:
        logger_config = json.load(config_file)
        logging.config.dictConfig(logger_config)


def create_log_directory(dir_name="log"):

    if not os.path.exists(dir_name):
        os.makedirs(dir_name)